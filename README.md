# Introduction
This is a simple framework for testing Rest API service for searching products.
Base api is https://waarkoop-server.herokuapp.com.

# Technology Stack
- Java
- Serenity BDD
- Rest Assured
- Cucumber
- Maven
- JUnit

# Prerequisites
- Java 11
- Maven - Dependency Manager
- Serenity 4.0.21

# Framework & Design Considerations
Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured.
API calls & validations are made using RestAssured and SerenityRest which is a wrapper on top of RestAssured.
Tests are written in BDD Gherkin format in Cucumber feature files, and it is represented as a living documentation in the test report.
Each domain package consist of an Action class where API parsers are defined.
These domain models are called from a step-definitions class which are in-turn called from BDD tests.
APIs responses parses using POJO's classes and SerenityRestAssured methods.
Tests were divided by tags: 

- @SmokeTest - contains positive scenario and one negative scenario.

- @RegressionTest - contains all tests

# Project directory structure
src
main.java 
- pojos - Package for POJO's files


test.java
- apis - Package for APIs classes
- stepdefinitions  - Step definitions for the BDD feature file
- utils  - Common utility methods
- TestRunner - Runner file for your tests

- resources
  - features.search - Feature files directory
  - test-data - Contains test-data json file
  - serenity.conf - Serenity configuration file
  - logback-test.xml - for debug logs 

pom.xml - _xml - file containing information about the project and configuration details used by Maven to build the project.

# Installation and Test Execution
Run in any IDE

$ mvn clean install

# Execute Tests
Run the following command in Terminal to execute tests:

$mvn clean verify

# Test Report
**Artifacts (reports)** attached in the pipeline expires in 24hrs.(configuration in .gitlab-ci.yml)

**Serenity reports** you can find in the following directory of the Project.
target/site/serenity/index.html

#CI Execution

# Gitlab report
Go to CI/CD --> Pipelines, select the pipeline --> 
Go to download artifact, open serenity report.

or

Go to Pages and open the report there https://gitlab.com/osipchyknastassia/serenityRestTestTask/pages

# Additional configurations
Additional command line parameters can be passed for switching the application environment using below commands

Run 
$mvn clean verify -Denvironment=DEV

Environment configurations is specified in the file test/resources/serenity.conf. Now all environments have the same path.

Also, It is possible to run different sutes using tags. For example positive tests or negative.
mvn clean verify -Dcucumber.options="--tags=@positive"
mvn clean verify -Dcucumber.options="--tags=@negative"


Environment configurations is specified in the file test/resources/serenity.conf. Now all environments have the same path.

# Changes done in the given project:
- Removed all the gradle related files since we are using Maven
- Under pom.xml made below changes :
    - Changed groupId and artifactId project identifier
    - Upgrade serenity version to 4.0.21
    - Remove some setting for clean up
    - Restructure code: for better maintenance

- Removed some not valid classes
- Updated step definitions and moved business logic to steps to add extra layer of abstraction.
- Added utils for working with JSON and Lists.
- Added report.customfields.environment =${environment} - to print the environment on the report

