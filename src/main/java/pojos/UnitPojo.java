package pojos;

import lombok.Data;

@Data
public class UnitPojo {
    private double value;
    private String measurement;

    public UnitPojo(String fullName) {
        String[] values = fullName.split(" ");
        value = Double.parseDouble(values[0]);
        measurement = values[1];
    }
}
