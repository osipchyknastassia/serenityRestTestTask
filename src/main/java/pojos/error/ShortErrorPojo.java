package pojos.error;

import lombok.Data;

@Data
public class ShortErrorPojo {
    String detail;
}
