package pojos.error;

import lombok.Data;

@Data
public class DetailPojo {
	private boolean error;
	private String message;
	private String requested_item;
	private String served_by;
}
