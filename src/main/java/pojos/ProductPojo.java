package pojos;

import lombok.Data;

@Data
public class ProductPojo {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private Double price;
    private UnitPojo unit;
    private Boolean isPromo;
    private String promoDetails;
    private String image;
}


