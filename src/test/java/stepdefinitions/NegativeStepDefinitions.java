package stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;
import pojos.error.DetailPojo;
import pojos.error.ErrorPojo;
import pojos.error.ShortErrorPojo;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.assertj.core.api.Assertions.assertThat;

public class NegativeStepDefinitions {
    private DetailPojo actualError;

    /**
     * Step parses response body to error format and throw the exception if format is different as expected.
     */
    @When("I see that error message is in error format")
    public void parseMessage() {
        actualError = then().extract().as(ErrorPojo.class).getDetail();
    }

    /**
     * That step verifies that :
     * isError field is true
     * getServed_by field is equals: <a href="https://waarkoop.com"/>.
     */
    @Then("I see the error fields has expected data")
    public void verifyFields() {
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(actualError.isError()).isTrue();
        softAssertions.assertThat(actualError.getServed_by()).isEqualTo("https://waarkoop.com");
        softAssertions.assertAll();
    }

    /**
     * TThat step verify is message field equals expected error message.
     */
    @Then("I see the error message is {string}")
    public void verifyTheErrorMessage(String expectedErrorMessage) {
        assertThat(actualError.getMessage()).withFailMessage("Error message was wrong")
                .isEqualTo(expectedErrorMessage);
    }

    /**
     * Verify that Requested_item field equals searched one
     * @param searchedItem - product type that user searched
     */
    @Then("I see the requested item is equals of the searched product: {string}")
    public void errorMessageContainsRequestedItem(String searchedItem) {
        assertThat(actualError.getRequested_item()).withFailMessage("wrong requested item")
                .isEqualTo(searchedItem);
    }

    /**
     * Parse response to the shortError pojo.
     * Verify that 'detail' is equals expected message.
     * @param expDetailMsg - expected error message
     */
    @Then("I see that short error contains message {string}")
    public void verifyShortErrorMessageHasText(String expDetailMsg) {
        String errorMessage = then().extract().as(ShortErrorPojo.class).getDetail();
        assertThat(errorMessage)
                .withFailMessage("Detail text was wrong")
                .isEqualTo(expDetailMsg);
    }
}
