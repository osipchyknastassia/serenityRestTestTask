package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.annotations.Steps;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import pojos.ProductPojo;
import java.util.List;
import apis.ProductApi;
import utils.JsonUtils;
import static net.serenitybdd.rest.SerenityRest.then;

public class SearchStepDefinitions {
    private static final String ID_PATTERN = "^\\d{8,}$";
    private static final String EXPECTED_PATH = "https://www.coop.nl/product/";
    @Steps
    public ProductApi productApi;
    private String searchItem;
    private List<ProductPojo> productsList;
    private final SoftAssertions softAssertions = new SoftAssertions();

    /**
     * Send GET request to search all products by type. F.ex. cola or pasta.
     *
     * @param searchType - product type
     */
    @Given("I search product: {string}")
    public void userSearchProduct(String searchType) {
        this.searchItem = searchType;
        productApi.searchProductsByType(this.searchItem);
    }

    /**
     * Parse response to Product list.
     * Throw UnrecognizedPropertyException exception if format is wrong.
     */
    @Then("I see the results as a list of products")
    public void parseToListOfProducts() {
        productsList = then().extract().body().jsonPath().getList("", ProductPojo.class);
    }

    /**
     * Steep verifies that  products list isn't empty
     */
    @Then("The product list is not empty")
    public void verifyTheListIsNotEmpty() {
        Assertions.assertThat(productsList)
                .withFailMessage(String.format("Product list shouldn't be empty for %s", searchItem))
                .isNotEmpty();
    }

    /**
     * Checks that are done in that method:
     * title, brand, provider and image fields is not empty
     * all urls contain link with ID
     */
    @Then("I see all fields has valid data")
    public void verifyValidData() {
        for (ProductPojo product : productsList) {
            softAssertions.assertThat(product.getBrand()).isNotEmpty();
            softAssertions.assertThat(product.getProvider()).isNotEmpty();
            softAssertions.assertThat(product.getTitle()).isNotEmpty();
            softAssertions.assertThat(product.getImage()).isNotEmpty();

            String url = product.getUrl();
            softAssertions.assertThat(url).startsWith(EXPECTED_PATH);
            softAssertions.assertThat(url.replace(EXPECTED_PATH, "").matches(ID_PATTERN)).isTrue();
        }
        softAssertions.assertAll();
    }

    /**
     * Verify that all products in list has price more than minPrice
     *
     * @param minPrice - price should be more than that value
     */
    @Then("I see the price is more then {int}")
    public void verifyPriceIsMoreThen(int minPrice) {
        productsList.forEach(product ->
                softAssertions.assertThat(product.getPrice())
                        .withFailMessage(String.format("Price was wrong for %s", product.getTitle()))
                        .isGreaterThan(minPrice));
        softAssertions.assertAll();
    }

    /**
     * Verify that all products in list has weight or volume more than minValue
     * And measurement is equals measurement or measurement2
     *
     * @param minValue     - weight or volume minimum value
     * @param measurement  - possible expected measurement value
     * @param measurement2 - second possible expected measurement value
     */
    @Then("I see all units are more then {int} with measurement {string} or {string}")
    public void unitIsMoreThenWithMeasurement(int minValue, String measurement, String measurement2) {
        for (ProductPojo product : productsList) {
            String actualMeasurement = product.getUnit().getMeasurement();
            softAssertions.assertThat(product.getUnit().getValue())
                    .withFailMessage(String.format("Unit minWeigh is %s", product.getTitle())).isGreaterThan(minValue);
            softAssertions.assertThat(actualMeasurement)
                    .withFailMessage(String.format("%s. Unit measurement is %s or %s", product.getTitle(), measurement, measurement2))
                    .containsAnyOf(measurement, measurement2);
        }
        softAssertions.assertAll();
    }

    /**
     * Verify that all products in list has expected product type in title.
     * Product type can have different names, that were mapped in test-data/Products.json file.
     * So test verifies, that title contains at least one of expected names for the product.
     *
     * @param expProduct - product type that user found
     */
    @Then("all products are {string}")
    public void allProductsIsExpectedOne(String expProduct) {
        List<String> expectedProducNameList = JsonUtils.getProductNamesInLowerCase(expProduct);
        for (ProductPojo product : productsList) {
            boolean isContains = expectedProducNameList.stream().anyMatch(product.getTitle().toLowerCase()::contains);
            softAssertions.assertThat(isContains).withFailMessage(
                    String.format("title  %s doesn't contain %s1", product.getTitle(), expectedProducNameList))
                    .isTrue();
        }
        softAssertions.assertAll();
    }
}


