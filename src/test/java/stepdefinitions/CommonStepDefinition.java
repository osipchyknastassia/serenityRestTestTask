package stepdefinitions;

import io.cucumber.java.en.Then;
import static net.serenitybdd.rest.SerenityRest.then;

public class CommonStepDefinition {

    /**
     * Common step that verify status code of current response
     * @param expectedStatusCode  - expected status code
     */
    @Then("I see the status code is {int}")
    public void verifyStatusCode(int expectedStatusCode) {
        then().statusCode(expectedStatusCode);
    }
}
