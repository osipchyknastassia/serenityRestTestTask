package apis;


public class ProductApi extends BaseApi{
	private final String basePath = "api/v1/search/demo/";

	/**
	 * Send GET request with productType to get list of products by type
	 * @param searchType - type of product. F.ex. apple or cola
	 */
	public void searchProductsByType(String searchType) {
		baseRequest.basePath(basePath).get(searchType);
	}
}
