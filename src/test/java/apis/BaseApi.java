package apis;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.model.environment.SystemEnvironmentVariables;
import net.thucydides.model.util.EnvironmentVariables;

import static net.serenitybdd.model.environment.EnvironmentSpecificConfiguration.from;

public class BaseApi {
	protected RequestSpecification baseRequest;
	
	/**
	 * setup base Request for SerenityRest(RestAssured) for all API classes
	 */
	public BaseApi() {
		EnvironmentVariables objEnvVar = SystemEnvironmentVariables.createEnvironmentVariables();
		String baseUrl = from(objEnvVar)
				.getProperty("baseurl");
		RequestSpecification uri = new RequestSpecBuilder()
				.setBaseUri(baseUrl)
				.setBasePath("api")
				.setContentType("application/json")
				.build();
		baseRequest  = SerenityRest.given().log().uri().spec(uri);
	}
}
