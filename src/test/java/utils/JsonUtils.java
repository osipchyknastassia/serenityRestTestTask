package utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class JsonUtils {

    public static final String PRODUCT_NAMES_PATH = "src/test/resources/test-data/Products.json";

    /**
     * Get all possible product names by type from test-data/Products.json file
     * @param product - product type to ge all names
     * @return List of products names in
     */
    public static List<String> getProductNamesInLowerCase(String product) {
        String jsonString;
        try {
            jsonString = readFileAsString(PRODUCT_NAMES_PATH);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray names  = jsonObject.getJSONArray(product);
            return  jsonArrayToListLowerCase(names);
    }

    /**
     * @param jsonArray - array of values in String format
     * @return list of string in lower case
     */
    private static List<String> jsonArrayToListLowerCase(JSONArray jsonArray) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            list.add(jsonArray.getString(i).toLowerCase());
        }
        return list;
    }

    /**
     * @param filePath - path of json file
     * @return String
     * @throws IOException - if file doesn't exist
     */
    public static String readFileAsString(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return new String(Files.readAllBytes(path));
    }
}
