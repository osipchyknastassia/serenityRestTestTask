Feature: Search for the product

  @positive
  Scenario Outline: Search for an existing product
    When I search product: '<product>'
    Then I see the status code is 200
    And I see the results as a list of products
    And The product list is not empty
    And I see all fields has valid data
    And I see the price is more then 0
    And I see all units are more then 0 with measurement '<measurement>' or '<measurement2>'
    And all products are '<product>'
    Examples:
      | product | measurement | measurement2 |
      | pasta   | g           | kg           |
      | cola    | ml          | l            |
      | apple   | g           | kg           |
      | orange  | g           | kg           |
      ### bugs should be created in jira for apple and orange.

  @negative
  Scenario: Search for not existing product
    When I search product: 'car'
    Then I see the status code is 404
    And I see that error message is in error format
    And I see the error message is 'Not found'
    And I see the error fields has expected data
    And I see the requested item is equals of the searched product: 'car'

  @negative
  Scenario: Send empty request
    When I search product: ''
    Then I see the status code is 401
    Then I see that short error contains message 'Not authenticated'
